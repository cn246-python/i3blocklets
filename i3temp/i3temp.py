#!/usr/bin/env python3

import re
import sys
import subprocess

# Set variables
sensor_chip = "coretemp-isa-0000"
temp_warn = 70
temp_crit = 90
temp = -9999

# Get chip temperature
sensor_out = subprocess.run(args=["sensors", "-u", sensor_chip],
        universal_newlines=True,
        stdout=subprocess.PIPE)

# Split output into list
sensor_lines = sensor_out.stdout.splitlines()

# Set regex to get value
r = re.compile("^\s+temp1_input:\s+[\+]*([\-]*\d+)")

# Use regex on list
sensor_lines = list(filter(r.match, sensor_lines))

# Print temperature
for temp in sensor_lines:
    temp = float(temp.split(" ")[3])
    temp = int(temp)
    if temp == -9999:
        print("Cannot find temperature")
    else: 
        print(f'{temp}°C\n')

    # Print color, if needed
    if temp >= temp_crit:
        print("#FF0000\n")
        sys.exit(33)
    elif temp >= temp_warn:
        print("#FFFC00\n")
